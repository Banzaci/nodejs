module.exports = function( app ) {

	app.param('id', function( req, res, next, id ){
		req.id = id;
		next()
	})

	app.get('/', function ( req, res ) {
		res.render('index.ejs');
	});
	
}