
var express         = require('express'),
    app             = module.exports = express(),
    io              = require('socket.io'),
    port            = 3000,
    ms              = io.listen(app.listen(port)),
    servers         = {},
    CONNECTION      = "connection",
    DISCONNECT      = "disconnect",
    FUNC            = 'func',
    NOTIFY          = 'notify';

ms.on(CONNECTION, function(socket) {

    socket.on(FUNC, function(data){
        data.funcs.forEach(function(func){
            servers[func] = servers[func] || [];
            servers[func].push(socket);
        });
        console.info('New client connected (id=' + socket.id + ').');
    });

    socket.on(NOTIFY, function(data, fn) {
        var eventNames,
            eventName,
            key;

        try {
            if(data.server){
                eventNames = data.server.split(",");
                eventNames.forEach(function(eventName){
                    eventName = eventName.trim();
                    key = servers[eventName];
                    if(key){
                        key.forEach(function(server){
                            server.emit( eventName, data, fn );
                        });
                    }
                });
            }
        } catch(e){
            //Log to server
            console.log(e.message);
        }
    });

    socket.on('client', function(data, fn) {
        if(typeof fn === "function"){
            fn({ data:data });    
        }
    });

    socket.on(DISCONNECT, function() {
        var index,
            server,
            key;

        for(server in servers){
            if(servers.hasOwnProperty(server)){
                key = servers[server];
                key.forEach(function(s){
                    index = key.indexOf(s);
                    if(s.id === socket.id && index !== -1){
                        console.log( 'Disconnect on master: ', socket.id );
                        servers[server].splice(index, 1);
                    }
                });
            }
        }
    }); 
});

ms.set("transports", ['websocket', 'polling', 'htmlfile' ]);